#include <string>
#include <array>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <boost/asio/ip/host_name.hpp>
#include "json.hpp"
#include <regex>
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <locale>
#include <array>
#include <unistd.h>
#include <getopt.h>
#include "version.h"
#include "yaml-cpp/yaml.h"


// souce from https://stackoverflow.com/questions/4181784/how-to-set-socket-timeout-in-c-when-making-multiple-connections
#ifdef WIN32
    #include <winsock2.h>
#else
    #include <sys/socket.h>
#endif

#ifndef SOL_TCP
    #define SOL_TCP 6
#endif
#ifndef TCP_USER_TIMEOUT
    #define TCP_USER_TIMEOUT 18
#endif

using json = nlohmann::json;

using namespace std;

string configuration_path;

static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

bool file_exist(const char * filename){
    std::ifstream infile(filename);
    return infile.good();
}

string exec(const string & cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

bool check_ignore(const string hostname, const string proto) {
    if (configuration_path.length() == 0)
        return false;

    if (!file_exist(configuration_path.c_str())) {
        cerr << "Configuration file does not exists: " << configuration_path << endl;
        abort();
    }

    YAML::Node config = YAML::LoadFile(configuration_path);

    if(!config["ssl_discovery_ignore"])
        return false;

    const YAML::Node& ignore = config["ssl_discovery_ignore"];
    for (YAML::const_iterator it = ignore.begin(); it != ignore.end(); ++it) {
        const YAML::Node& ignore = *it;
        if (ignore["SERVERNAME"].as<string>() == hostname && proto == ignore["SERVERPROTO"].as<string>())
            return true;
    }
    return false;
}

bool port_is_open(const char * address, int portno){
    int sockfd;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    int timeout = 2000;
    setsockopt (sockfd, SOL_TCP, TCP_USER_TIMEOUT, (char*) &timeout, sizeof (timeout));
    if (sockfd < 0)
        return false;
    server = gethostbyname(address);

    if (server == nullptr)
        return false;

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
            server->h_length);

    serv_addr.sin_port = htons(portno);
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        close(sockfd);
        return false;
    } else {
        close(sockfd);
        return true;
    }
    return false;
}

string get_hostname(){
    return string(boost::asio::ip::host_name());
}

vector<json> check_imap_ports(){
    string hostname = get_hostname();
    std::array<int, 2> ports = {143, 993};
    vector<json> data_array;

    for(const int & port: ports){
        bool check_port = port_is_open(hostname.c_str(), port);
        if (check_port){
            json j;
            j["{#SERVERNAME}"] = hostname.c_str();
            j["{#SERVERPORT}"] = std::to_string(port);
            j["{#SERVERPROTO}"] = "imap";
            j["{#SERVERIGNORE}"] = check_ignore(hostname.c_str(), "imap");
            data_array.push_back(j);
        }
    }
    return data_array;
}

vector<json> check_pop3_ports(){
    string hostname = get_hostname();
    std::array<int, 2> ports = {110, 995};
    vector<json> data_array;

    for(const int & port: ports){
        bool check_port = port_is_open(hostname.c_str(), port);
        if (check_port){
            json j;
            j["{#SERVERNAME}"] = hostname.c_str();
            j["{#SERVERPORT}"] = std::to_string(port);
            j["{#SERVERPROTO}"] = "pop3";
            j["{#SERVERIGNORE}"] = check_ignore(hostname.c_str(), "pop3");
            data_array.push_back(j);
        }
    }
    return data_array;
}

vector<json> check_mongodb_ports(){
    string hostname = get_hostname();
    std::array<int, 3> ports = {27017, 27018, 27019};
    vector<json> data_array;

    for(const int & port: ports){
        bool check_port = port_is_open(hostname.c_str(), port);
        if (check_port){
            json j;
            j["{#SERVERNAME}"] = hostname.c_str();
            j["{#SERVERPORT}"] = std::to_string(port);
            j["{#SERVERPROTO}"] = "mongodb";
            j["{#SERVERIGNORE}"] = check_ignore(hostname.c_str(), "mongodb");
            data_array.push_back(j);
        }
    }
    return data_array;
}
vector<json> check_smtp_ports(){
    string hostname = get_hostname();
    std::array<int, 3> ports = {25, 587, 465};
    vector<json> data_array;

    for(const int & port: ports){
        bool check_port = port_is_open(hostname.c_str(), port);
        if (check_port){
            json j;
            j["{#SERVERNAME}"] = hostname.c_str();
            j["{#SERVERPORT}"] = std::to_string(port);
            j["{#SERVERPROTO}"] = "smtp";
            j["{#SERVERIGNORE}"] = check_ignore(hostname.c_str(), "smtp");
            data_array.push_back(j);
        }
    }
    return data_array;
}

vector<json> check_vhosts(const string & output_apachectl){
    std::regex rgx("port ([a-zA-Z0-9]+) namevhost ([_0-9a-zA-Z\\-\\.]+)");
    std::smatch matches;

    vector<json> data_array;

    std::sregex_iterator next(output_apachectl.begin(), output_apachectl.end(), rgx);
    std::sregex_iterator end;

    while (next != end) {
        std::smatch matches = *next;
        if (matches.size() > 2){
            json j;
            j["{#SERVERNAME}"] = matches[2];
            j["{#SERVERPORT}"] = matches[1];
            if (matches[1] == "80") {
                j["{#SERVERPROTO}"] = "http";
                j["{#SERVERIGNORE}"] = check_ignore(matches[2], "http");
            }
            else {
                j["{#SERVERPROTO}"] = "https";
                j["{#SERVERIGNORE}"] = check_ignore(matches[2], "https");
            }
            data_array.push_back(j);
        }
        next ++;
    }

    return data_array;
}

vector<json> check_defaults(const string & output_apachectl){
	// *:443                  24lcs.com (/etc/apache2/sites-enabled/24lcs.com.conf:37)
    std::regex rgx("\\*:([0-9]+)\\s*([_0-9a-zA-Z\\-\\.]+)\\s\\(.+:[0-9]*\\)");
    std::smatch matches;

    vector<json> data_array;

    std::sregex_iterator next(output_apachectl.begin(), output_apachectl.end(), rgx);
    std::sregex_iterator end;

    while (next != end) {
        std::smatch matches = *next;
        if (matches.size() > 2){
            json j;
            j["{#SERVERNAME}"] = matches[2];
            j["{#SERVERPORT}"] = matches[1];
            if (matches[1] == "80") {
                j["{#SERVERPROTO}"] = "http";
                j["{#SERVERIGNORE}"] = check_ignore(matches[2], "http");
            }
            else {
                j["{#SERVERPROTO}"] = "https";
                j["{#SERVERIGNORE}"] = check_ignore(matches[2], "https");
            }
            data_array.push_back(j);
        }
        next ++;
    }

    return data_array;
}

vector<json> check_apache(const string & bin_name){
    string output_apachectl(exec(bin_name + " -S"));
    vector<json> vhosts = check_vhosts(output_apachectl);
    vector<json> defaults_hosts = check_defaults(output_apachectl);

    vhosts.insert(vhosts.end(), defaults_hosts.begin(), defaults_hosts.end());
	return vhosts;
}

string remove_all_hash_comments (const string & input){
    string out;
    bool comment_detected = false;
    char append[] = "\0";

    for (unsigned i = 0; i < input.size(); i ++){
        if (input[i] == '#'){
            comment_detected = true;
        }
        else if ((input[i] == '\n' || input[i] == '\0') && comment_detected){
            append[0] = input[i];
            out.append(append);
            comment_detected = false;
        }else if(!comment_detected){
            append[0] = input[i];
            out.append(append);
        }
    }
    return out;
}

string get_only_block(const string & input, unsigned int start){
    int deep = 0;
    bool block_opened = false;
    unsigned int i = start;
    char append[] = "\0";
    string out;

    while (true){
        if (i >= input.size()){
            break;
        }
        if (input[i] == '{'){
            start = i;
            deep ++;
            block_opened = true;
        }else
        if (input[i] == '}')
            deep --;
        if (deep == 0 && block_opened == true){
            append[0] = input[i];
            out.append(append);
            break;
        }
        if(block_opened == true){
            append[0] = input[i];
            out.append(append);
        }
        i ++;
    }
    return out;
}

string get_port_from_server_block(const string & server_block){
    std::smatch m;
    std::regex rgx("listen\\s*([0-9]+)");

    string ret("80");

    std::regex_search (server_block, m, rgx);

    if (m.size() > 1){
        ret = m[1];
    }

    return ret;
}

vector<string> get_server_names_from_server_block(const string & server_block){
    std::smatch m;
    std::regex rgx("server_name\\s*([_0-9a-zA-Z\\-\\.\\*\\s]+)+");

    vector<string> ret;
    string match;

    std::regex_search (server_block, m, rgx);

    for (unsigned i = 0; i < m.size(); ++ i) {
        if (m.size() > 1){
            match = m[1];
            vector<string> results;
            boost::split(results, match, [](char c){return c == ' ';});
            for (auto i = results.begin(); i != results.end(); ++ i){
                string _append(*i);
                trim(_append);
                if (_append != ""){
                    if((std::find(ret.begin(), ret.end(), _append) != ret.end()) == false) {
                        ret.push_back(_append);
                    }
                }
            }
        }
    }

    return ret;
}

vector<json> check_nginx(){
    vector<json> data_array;
    string output_nginxt(exec(string("/usr/sbin/nginx -T")));
    std::regex rgx("server\\s");

    output_nginxt = remove_all_hash_comments(output_nginxt);

    std::sregex_iterator next(output_nginxt.begin(), output_nginxt.end(), rgx);
    std::sregex_iterator end;

    while (next != end) {
        std::smatch matches = *next;
        for (unsigned i = 0; i < matches.size(); ++ i) {
            string server_block = get_only_block(output_nginxt, matches.position(i));
            string port = get_port_from_server_block(server_block);
            vector<string> server_names = get_server_names_from_server_block(server_block);
            for (auto i = server_names.begin(); i != server_names.end(); ++i){
                if (*i != "_"){
                    json j;
                    string hostname;
                    string protocol;
                    if (i->rfind("*.", 0) == 0)
                        hostname = i->replace(0, 1, "WILDCARD-DOMAIN");
                    else
                        hostname = *i;
                    j["{#SERVERNAME}"] = hostname;
                    j["{#SERVERPORT}"] = port;
                    if (port == "80")
                        protocol = "http";
                    else
                        protocol = "https";
                    j["{#SERVERPROTO}"] = protocol;
                    j["{#SERVERIGNORE}"] = check_ignore(hostname, protocol);
                    data_array.push_back(j);
                }
            }
        }
        next ++;
    }
    return data_array;
}

json check_gitlab(){
    vector<json> data_array;
    string output_gitlab_ctl(exec(string("/usr/bin/gitlab-ctl show-config")));
    json j;

    std::smatch m;
    std::regex rgx("\"external-url\":\\s\"(http|https)?://([a-zA-Z\\.0-9\\-]*)");
    std::regex_search (output_gitlab_ctl, m, rgx);

    if (m.size() > 2){
        j["{#SERVERNAME}"] = m[2];
        j["{#SERVERPORT}"] = string("80");
        if (m[1] == "https"){
            j["{#SERVERPORT}"] = string("443");
        }
        j["{#SERVERPROTO}"] = m[1];
        j["{#SERVERIGNORE}"] = check_ignore(m[2], m[1]);
    }

    return j;
}

vector<json> check_proxmox(){
    string hostname = get_hostname();
    int port = 8006;
    vector<json> data_array;

    bool check_port = port_is_open(hostname.c_str(), port);
    if (check_port){
        json j;
        j["{#SERVERNAME}"] = hostname.c_str();
        j["{#SERVERPORT}"] = port;
        j["{#SERVERPROTO}"] = "https";\
        j["{#SERVERIGNORE}"] = check_ignore(hostname.c_str(), "https");
        data_array.push_back(j);
    }
    return data_array;
}

void PrintHelp() {
    cout <<
            "-c, --config <path>:     Path to a configuration YAML file.\n"
            "-h, --help:              Print this help message.\n"
            "-V, --version:           Print version of discover_ssl.\n";
}

int main (int argc, char* argv[]) {
    int option;

    const struct option longOptions[] = {
        {"version", no_argument, nullptr, 'V'},
        {"config", required_argument, nullptr, 'c'},
        {"help", no_argument, nullptr, 'h'},
        {nullptr, 0, nullptr, 0}
    };

    while ((option = getopt_long(argc, argv, "hVc:", longOptions, nullptr)) != -1) {

        switch (option) {
            case 'V':
                cout << "Version: " << SSL_DISCOVERY_VERSION << endl;
                return 0;
            case 'c':
                configuration_path = string(optarg);
                break;
            case 'h':
                PrintHelp();
                return 0;
            case '?':
                PrintHelp();
                return 1;
            default:
                break;
        }
    }

    vector<json> data_array;

    if (file_exist("/etc/dovecot")){
        vector<json> imap_additions = check_imap_ports();
        data_array.insert(data_array.end(), imap_additions.begin(), imap_additions.end());

        vector<json> pop3_additions = check_pop3_ports();
        data_array.insert(data_array.end(), pop3_additions.begin(), pop3_additions.end());
    }

    if (file_exist("/etc/mongod.conf")){
        vector<json> additions = check_mongodb_ports();
        data_array.insert(data_array.end(), additions.begin(), additions.end());
    }

    if (file_exist("/etc/postfix")){
        vector<json> additions = check_smtp_ports();
        data_array.insert(data_array.end(), additions.begin(), additions.end());
    }

    if (file_exist("/usr/sbin/apache2ctl")){
        vector<json> additions = check_apache("/usr/sbin/apache2ctl");
        data_array.insert(data_array.end(), additions.begin(), additions.end());
    }else if(file_exist("/usr/sbin/apachectl")){
        vector<json> additions = check_apache("/usr/sbin/apachectl");
        data_array.insert(data_array.end(), additions.begin(), additions.end());
    }else if(file_exist("/usr/local/apache2/bin/apachectl")){
        vector<json> additions = check_apache("/usr/local/apache2/bin/apachectl");
        data_array.insert(data_array.end(), additions.begin(), additions.end());
    }

    if (file_exist("/usr/sbin/nginx")){
        vector<json> additions = check_nginx();
        data_array.insert(data_array.end(), additions.begin(), additions.end());
    }

    if (file_exist("/usr/bin/gitlab-ctl")){
        json add = check_gitlab();
        if (add != nullptr)
            data_array.push_back(add);
    }

    if (file_exist("/etc/pve/")){
        json add = check_proxmox();
        if (add != nullptr)
            data_array.push_back(add);
    }

    json result;
    result["data"] = data_array;
    cout << result << endl;
    result.clear();
	return 0;
}
