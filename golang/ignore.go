package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	SslDiscoveryIgnore []struct {
		ServerName  string `yaml:"SERVERNAME"`
		ServerProto string `yaml:"SERVERPROTO"`
	} `yaml:"ssl_discovery_ignore"`
}

func checkIgnore(hostname, proto string) bool {

	if len(configurationPath) == 0 {
		return false
	}

	if _, err := os.Stat(configurationPath); os.IsNotExist(err) {
		fmt.Printf("Configuration file does not exist: %s\n", configurationPath)
		os.Exit(1)
	}

	fileBytes, err := ioutil.ReadFile(configurationPath)
	if err != nil {
		fmt.Printf("Error reading config file: %s\n", err)
		os.Exit(1)
	}

	var config Config
	err = yaml.Unmarshal(fileBytes, &config)
	if err != nil {
		fmt.Printf("Error parsing YAML file: %s\n", err)
		os.Exit(1)
	}

	if config.SslDiscoveryIgnore == nil {
		return false
	}

	for _, ignore := range config.SslDiscoveryIgnore {
		if ignore.ServerName == hostname && proto == ignore.ServerProto {
			return true
		}
	}

	return false
}
