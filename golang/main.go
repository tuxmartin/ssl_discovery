package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
)

type ServerData struct {
	ServerName   string `json:"{#SERVERNAME}"`
	ServerPort   string `json:"{#SERVERPORT}"`
	ServerProto  string `json:"{#SERVERPROTO}"`
	ServerIgnore bool   `json:"{#SERVERIGNORE}"`
}

func ServerDataContains(serverArray []ServerData, server ServerData) bool {
	for _, s := range serverArray {
		if s.ServerName == server.ServerName &&
			s.ServerPort == server.ServerPort &&
			s.ServerProto == server.ServerProto &&
			s.ServerIgnore == server.ServerIgnore {
			return true
		}
	}
	return false
}

func getHostname() (string, error) {
	host, err := os.Hostname()
	if err != nil {
		return "", err
	}
	return host, nil
}

func portIsOpen(host string, port int) bool {
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		return false
	}
	conn.Close()
	return true
}

func fileExist(filename string) bool {
	_, err := os.Stat(filename)
	return !os.IsNotExist(err)
}

func contains(slice []string, item string) bool {
	for _, a := range slice {
		if a == item {
			return true
		}
	}
	return false
}

const (
	sslDiscoveryVersion = "SSL_DISCOVERY_VERSION_PLACEHOLDER"
)

var configurationPath string

func printHelp() {
	fmt.Println(`
-c, --config <path>:    Path to a configuration YAML file.
-h, --help:             Print this help message.
-V, --version:          Print version of discover_ssl.`)
}

func main() {
	versionPtr := flag.Bool("V", false, "version")
	configPtr := flag.String("c", "", "config")
	helpPtr := flag.Bool("h", false, "help")

	flag.Parse()

	configurationPath = *configPtr

	if *versionPtr {
		fmt.Println("Version: ", sslDiscoveryVersion)
		return
	}

	if *helpPtr {
		printHelp()
		return
	}

	serverDataList := make([]ServerData, 0)

	if fileExist("/etc/dovecot") {
		dovecotData, err := checkPop3Ports()
		if err != nil {
			panic(err)
		}
		serverDataList = append(serverDataList, dovecotData...)

		popData, err := checkPop3Ports()
		if err != nil {
			panic(err)
		}
		serverDataList = append(serverDataList, popData...)
	}

	if fileExist("/etc/mongod.conf") {
		mongodbData, err := checkMongodbPorts()
		if err != nil {
			panic(err)
		}
		serverDataList = append(serverDataList, mongodbData...)
	}

	if fileExist("/etc/postfix") {
		postfixData, err := checkSmtpPorts()
		if err != nil {
			panic(err)
		}
		serverDataList = append(serverDataList, postfixData...)
	}

	apacheBinPaths := []string{
		"/usr/sbin/apache2ctl",
		"/usr/sbin/apachectl",
		"/usr/local/apache2/bin/apachectl",
	}
	for _, path := range apacheBinPaths {
		if fileExist(path) {
			additions, err := checkApache(path)
			if err != nil {
				log.Fatalf("Error checking Apache: %v", err)
			}
			serverDataList = append(serverDataList, additions...)
			break
		}
	}

	if fileExist("/usr/sbin/nginx") {
		nginxData, err := checkNginx()
		if err != nil {
			panic(err)
		}
		serverDataList = append(serverDataList, nginxData...)
	}

	if fileExist("/usr/bin/gitlab-ctl") {
		gitlabData, err := checkGitlab()
		if err != nil {
			panic(err)
		}
		serverDataList = append(serverDataList, gitlabData)
	}

	if fileExist("/etc/pve/") {
		proxmoxData, err := checkProxmox()
		if err != nil {
			panic(err)
		}
		serverDataList = append(serverDataList, proxmoxData...)
	}

	jsonData, err := json.Marshal(serverDataList)
	if err != nil {
		panic(err)
	}

	result := make(map[string]interface{})
	result["data"] = serverDataList

	jsonData, err = json.Marshal(result)
	if err != nil {
		log.Fatalf("Failed to marshal JSON: %v", err)
	}

	fmt.Println(string(jsonData))
}
