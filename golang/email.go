package main

import (
	"fmt"
	"os"
)

func checkImapPorts() ([]ServerData, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	ports := []int{143, 993}
	var dataArr []ServerData

	for _, port := range ports {
		if portIsOpen(hostname, port) {
			dataArr = append(dataArr, ServerData{ServerName: hostname, ServerPort: fmt.Sprintf("%d", port), ServerProto: "imap", ServerIgnore: checkIgnore(hostname, "imap")})
		}
	}
	return dataArr, nil
}

func checkPop3Ports() ([]ServerData, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	ports := []int{110, 995}
	var dataArr []ServerData

	for _, port := range ports {
		if portIsOpen(hostname, port) {
			dataArr = append(dataArr, ServerData{ServerName: hostname, ServerPort: fmt.Sprintf("%d", port), ServerProto: "pop3", ServerIgnore: checkIgnore(hostname, "pop3")})
		}
	}
	return dataArr, nil
}

func checkSmtpPorts() ([]ServerData, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	ports := []int{25, 587, 465}
	var dataArr []ServerData

	for _, port := range ports {
		if portIsOpen(hostname, port) {
			dataArr = append(dataArr, ServerData{ServerName: hostname, ServerPort: fmt.Sprintf("%d", port), ServerProto: "smtp", ServerIgnore: checkIgnore(hostname, "smtp")})
		}
	}
	return dataArr, nil
}
